/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nope;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Swampy
 */
public class ModelJudete extends AbstractTableModel{
    
    private String comanda;
    private Connection conexiune;
    private ResultSet r;
    private Statement s;
    private int m,n;
    private String[] coloane;

    public ModelJudete(String comanda, Connection conexiune) throws Exception {
        this.comanda = comanda;
        this.conexiune = conexiune;
        s = conexiune.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        r = s.executeQuery(comanda);
        ResultSetMetaData rm = r.getMetaData();
        m = rm.getColumnCount();
        if (r.last()) {
            n = r.getRow();
        }
        coloane=new String[m];
        for(int i=0;i<m;i++){
            coloane[i]=rm.getColumnName(i+1);
        }
        //r.close();
        //s.close();
    }
    
    

    @Override
    public int getRowCount() {
        return n;//To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getColumnCount() {
        return m; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            if (r.absolute(rowIndex + 1)) {
                return r.getObject(columnIndex + 1);
            } else {
                return null;
            }
        } catch (Exception ex) {
            System.out.println(ex);
            return null;
        } //To change body of generated methods, choose Tools | Templates.
    }
    
     @Override
    public String getColumnName(int column) {
        return coloane[column];
    }
    
    
    
}
