package classes;

public interface Teritoriu {

	public void descriere();
	public void add(Teritoriu teritoriu);
	public void remove(Teritoriu teritoriu);
	public Teritoriu getChild(int index);
	
}
