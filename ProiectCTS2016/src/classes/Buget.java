package classes;

import java.sql.Date;
import java.util.ArrayList;


public abstract class Buget {
    protected int idBuget;
    protected int idJudet;
    protected Date data;
    protected double venituriTotale;
    protected double cheltuieliTotale;
    protected ArrayList<Venit> venituri = null;
    protected ArrayList<Cheltuiala> cheltuieli =  null;
    protected IPrelucrare entitate;;
    
    
    public Buget(int idBuget, int idJudet, Date data) {
        this.idBuget = idBuget;
        this.idJudet = idJudet;
        this.data = data;
        this.venituriTotale = 0.0;
        this.cheltuieliTotale = 0.0;
    }
    
    //strategy
    public void setPrelucrare(IPrelucrare entitate){
    	this.entitate= entitate;
    }
    
    public IPrelucrare getPrelucrare(){
    	return this.entitate;
    }

    public void aduna(double valoare) throws ExceptieNumarMare, ExceptieNumarMic{
		 	
    }
    
    public void scade(double valoare) throws ExceptieNumarMic, ExceptieNumarMare{
    	
    }
    
    
	public IValoare createObject(String tip) {
		// TODO Auto-generated method stub
		return null;
	}



	public Buget(int idJudet) {
        this.idJudet = idJudet;
        this.venituriTotale = 0.0;
        this.cheltuieliTotale = 0.0;
    }
    

    public int getIdBuget() {
        return idBuget;
    }

    public int getIdJudet() {
        return idJudet;
    }

    public Date getData() {
        return data;
    }

    public double getVenituriTotale() {
        return venituriTotale;
    }

    public double getCheltuieliTotale() {
        return cheltuieliTotale;
    }

    public void setIdBuget(int idBuget) {
        this.idBuget = idBuget;
    }

    public void setIdJudet(int idJudet) {
        this.idJudet = idJudet;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public void setVenituriTotale(double venituriTotale) {
        this.venituriTotale = venituriTotale;
    }

    public void setCheltuieliTotale(double cheltuieliTotale) {
        this.cheltuieliTotale = cheltuieliTotale;
    }
   
    
}
