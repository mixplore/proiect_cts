package classes;


import javax.swing.*;

public class FereastraBuilder extends FereastraAbstractBuilder{

	
	FereastraCustom fereastra;
	
	
	public FereastraBuilder(String tipFereastra){
		
		this.fereastra = new FereastraCustom(tipFereastra);
	}
	
	@Override
	public FereastraBuilder adaugaButon(JButton buton) {
		
		this.fereastra.setButonLogin(buton);
		return this;
		
	}

	@Override
	public FereastraBuilder adaugaLabel(JLabel eticheta) {

		this.fereastra.setEticheta(eticheta);
		return this;
		
	}

	@Override
	public FereastraBuilder adaugaCasetaDeText(JTextField casetaText) {

		this.fereastra.add(casetaText);
		return this;
		
	}
	
	public FereastraCustom buildFereastra(){
		return this.fereastra;
	}

}
