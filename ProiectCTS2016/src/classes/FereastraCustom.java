package classes;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.*;
import java.awt.BorderLayout;

public class FereastraCustom extends JFrame {

	
	ArrayList<JButton> butoane = null;
	ArrayList<JLabel> etichete = null;
	ArrayList<JTextField> campuriText = null;
	
	
	JButton butonLogin;
	JLabel eticheta;
	JTextField textBox;
	
	
	public FereastraCustom(String tipFereastra) {
		
		this.setTitle(tipFereastra);
		setSize(500, 400);
        setBackground(Color.LIGHT_GRAY);
        //setLocationRelativeTo(null);
        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
       
	}

	public JButton getButonLogin() {
		return butonLogin;
	}
	
	public void setButonLogin(JButton buton){
		
		this.butonLogin = buton;
		getContentPane().add(butonLogin);
		
	}



	public JLabel getEticheta() {
		return eticheta;
	}



	public void setEticheta(JLabel eticheta) {
		this.eticheta = eticheta;
		getContentPane().add(eticheta);
	}



	public JTextField getTextBox() {
		return textBox;
	}



	public void setTextBox(JTextField textBox) {
		this.textBox = textBox;
		getContentPane().add(textBox);
	}




	
	
	
}
