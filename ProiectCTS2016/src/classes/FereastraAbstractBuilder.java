package classes;

import javax.swing.*;

public abstract class FereastraAbstractBuilder {

	public abstract FereastraBuilder adaugaButon(JButton buton);
	public abstract FereastraBuilder adaugaLabel(JLabel eticheta);
	public abstract FereastraBuilder adaugaCasetaDeText(JTextField casetaText);
	

}
