package classes;

import javax.swing.JFrame;

public class FereastraUnica extends JFrame {

	private static FereastraUnica instance = null;
	
	
	private FereastraUnica(String string) {
		this.setTitle(string);
	}

	public static FereastraUnica getInstance(String title){
		
		if(instance == null){
			instance = new FereastraUnica(title);
		}
		return instance;
	}
	
}
