package classes;

import java.util.ArrayList;

//composite
public class RegiuneDeDezvoltare implements Teritoriu {
    int idRegiune;
    String denumireRegiune;
    public ArrayList<Teritoriu> judete = new ArrayList<Teritoriu>();


    public RegiuneDeDezvoltare(int idZona, String denumireZona) throws ExceptieValoareNegativa {
    	if(idZona<0)
    		throw new ExceptieValoareNegativa();
        this.idRegiune = idZona;
        this.denumireRegiune = denumireZona;
    }

    public RegiuneDeDezvoltare(String denumireZona) {
        this.denumireRegiune = denumireZona;
    }  

    public int getIdRegiune() {
		return idRegiune;
	}

	public void setIdRegiune(int idRegiune) {
		this.idRegiune = idRegiune;
	}

	public String getDenumireRegiune() {
		return denumireRegiune;
	}

	public void setDenumireRegiune(String denumireRegiune) {
		this.denumireRegiune = denumireRegiune;
	}

	public ArrayList<Teritoriu> getJudete() {
		return judete;
	}

	public void setJudete(ArrayList<Teritoriu> judete) {
		this.judete = judete;
	}

	public int getIdZona() {
        return idRegiune;
    }

    public String getDenumireZona() {
        return denumireRegiune;
    }

    public void setIdZona(int idZona)throws ExceptieNumarMare, ExceptieValoareNegativa {
    	if(idZona > Integer.MAX_VALUE)
    		throw new ExceptieNumarMare("Valoare id prea mare!");
    	if(idZona < 0)
    		throw new ExceptieValoareNegativa();
        this.idRegiune = idZona;
    }

    public void setDenumireZona(String denumireZona) {
        this.denumireRegiune = denumireZona;
    }

	@Override
	public void descriere() {
		System.out.println("Regiune de dezvoltare: "+this.denumireRegiune);
		for(Teritoriu judet : judete)
		{
			System.out.print("  ");
			judet.descriere();
		}
		
	}

	@Override
	public void add(Teritoriu teritoriu) {
		this.judete.add(teritoriu);		
	}

	@Override
	public void remove(Teritoriu teritoriu) {
		this.judete.remove(teritoriu);
		
	}

	@Override
	public Teritoriu getChild(int index) {
		
		return this.judete.get(index);
	}


    
    
}
