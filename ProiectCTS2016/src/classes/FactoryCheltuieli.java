package classes;

import java.sql.Date;

public class FactoryCheltuieli extends Buget{

	public FactoryCheltuieli(int idJudet) {
		super(idJudet);
		// TODO Auto-generated constructor stub
	}
	
	public FactoryCheltuieli(int idBuget, int idJudet, Date data) {
		super(idBuget, idJudet, data);
		// TODO Auto-generated constructor stub
	}

	@Override
	public IValoare createObject(String tip) {
		
		return new Cheltuiala(tip);
	}
	
	@Override
	public void aduna(double valoare) throws ExceptieNumarMare, ExceptieNumarMic{
		
		if (valoare == Double.MAX_VALUE)
			throw new ExceptieNumarMare("valoare prea mare!");
		if (valoare == Double.MIN_VALUE)
			throw new ExceptieNumarMic("valoare prea mica!");
		this.cheltuieliTotale += valoare;
	}
	
	@Override
	public void scade(double valoare) throws ExceptieNumarMic, ExceptieNumarMare{
		
		if (valoare == Double.MAX_VALUE)
			throw new ExceptieNumarMare("valoare prea mare!");
		if (valoare == Double.MIN_VALUE)
			throw new ExceptieNumarMic("valoare prea mica!");
		this.cheltuieliTotale -=valoare;
	}

}
