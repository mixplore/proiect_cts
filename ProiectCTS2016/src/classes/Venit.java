package classes;

import java.sql.Date;


public class Venit implements IValoare {
    int idVenit;
    Date data;
    String tipVenit;
    double valoare;
    int idBuget;

    public Venit(int idVenit, Date data, String tipVenit, double valoare, int idBuget) {
        this.idVenit = idVenit;
        this.data = data;
        this.tipVenit = tipVenit;
        this.valoare = valoare;
        this.idBuget = idBuget;
    }

    public Venit(String tip) {
		this.tipVenit = tip;
	}

	public int getIdVenit() {
        return idVenit;
    }

    public Date getData() {
        return data;
    }

    public String getTipVenit() {
        return tipVenit;
    }

    public double getValoare() {
        return valoare;
    }

    public int getIdBuget() {
        return idBuget;
    }

    public void setIdVenit(int idVenit) {
        this.idVenit = idVenit;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public void setTipVenit(String tipVenit) {
        this.tipVenit = tipVenit;
    }

    public void setValoare(double valoare) {
        this.valoare = valoare;
    }

    public void setIdBuget(int idBuget) {
        this.idBuget = idBuget;
    }



	@Override
	public void afiseaza() {

		System.out.println("Venitul are valoarea de: "+ this.valoare);
		
	}
    
    
    
}
