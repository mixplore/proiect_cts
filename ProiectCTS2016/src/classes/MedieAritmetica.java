package classes;

public class MedieAritmetica implements IPrelucrare {

	@Override
	public double prelucreaza(double[] valori) {

		double suma = 0.0;
		if(valori != null)
		{
			for(int i=0;i<valori.length;i++)
				suma+=valori[i];
		}
		return suma/valori.length;
	}

}
