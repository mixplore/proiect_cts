package classes;


public class Judet implements Teritoriu {
    int idJudet;
    String numeJudet;
    int nrLocuitori;
    double suprafata;
    int idRegiune;

    public Judet() {
    }
    
    public Judet(int idRegiune) {
    	this.idRegiune = idRegiune;
    }

    public Judet(String denumire) {
    	
    	this.numeJudet = denumire;
    	this.idJudet = idJudet;
        this.nrLocuitori = 0;
        this.suprafata = 0;
        this.idRegiune = 1;
    }
    
    public Judet(int idJudet, String numeJudet, int nrLocuitori, double suprafata, int idZona) {
        this.idJudet = idJudet;
        this.numeJudet = numeJudet;
        this.nrLocuitori = nrLocuitori;
        this.suprafata = suprafata;
        this.idRegiune = idZona;
    }

    public int getIdJudet() {
        return idJudet;
    }

    public String getNumeJudet() {
        return numeJudet;
    }

    public int getNrLocuitori() {
        return nrLocuitori;
    }

    public double getSuprafata() {
        return suprafata;
    }

    public int getIdZona() {
        return idRegiune;
    }

    public void setIdJudet(int idJudet) {
        this.idJudet = idJudet;
    }

    public void setNumeJudet(String numeJudet) {
        this.numeJudet = numeJudet;
    }

    public void setNrLocuitori(int nrLocuitori) {
        this.nrLocuitori = nrLocuitori;
    }

    public void setSuprafata(double suprafata) {
        this.suprafata = suprafata;
    }

    public void setIdZona(int idZona) {
        this.idRegiune = idZona;
    }

    @Override
    public String toString() {
        return "idJudet=" + idJudet + ", numeJudet=" + numeJudet + ", nrLocuitori=" + nrLocuitori + ", suprafata=" + suprafata + ", idZona=" + idRegiune;
    }

	@Override
	public void descriere() {
		
		System.out.println(this.toString());
		
	}

	@Override
	public void add(Teritoriu teritoriu) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void remove(Teritoriu teritoriu) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Teritoriu getChild(int index) {
		// TODO Auto-generated method stub
		return null;
	}
    
    
    
}
