package classes;

import java.sql.Date;


public class Cheltuiala implements IValoare{
    int idCheltuiala;
    Date data;
    String tipCheltuiala;
    double valoare;
    int idBuget;

    public Cheltuiala(int idCheltuiala, Date data, String tipCheltuiala, double valoare, int idBuget) {
        this.idCheltuiala = idCheltuiala;
        this.data = data;
        this.tipCheltuiala = tipCheltuiala;
        this.valoare = valoare;
        this.idBuget = idBuget;
    }

    public Cheltuiala(String tip) {
		this.tipCheltuiala = tip;
	}

	public int getIdCheltuiala() {
        return idCheltuiala;
    }

    public Date getData() {
        return data;
    }

    public String getTipCheltuiala() {
        return tipCheltuiala;
    }

    public double getValoare() {
        return valoare;
    }

    public int getIdBuget() {
        return idBuget;
    }

    public void setIdCheltuiala(int idCheltuiala) {
        this.idCheltuiala = idCheltuiala;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public void setTipCheltuiala(String tipCheltuiala) {
        this.tipCheltuiala = tipCheltuiala;
    }

    public void setValoare(double valoare) {
        this.valoare = valoare;
    }

    public void setIdBuget(int idBuget) {
        this.idBuget = idBuget;
    }



	@Override
	public void afiseaza() {

		System.out.println("Cheltuiala are valoarea de: "+ this.valoare);
		
	}
    
    
    
}
