package classes;

import java.sql.Date;

public class FactoryVenituri extends Buget {

	public FactoryVenituri(int idJudet) {
		super(idJudet);
		// TODO Auto-generated constructor stub
	}

	public FactoryVenituri(int idBuget, int idJudet, Date data) {
		super(idBuget, idJudet, data);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public IValoare createObject(String tip) {
		
		return new Venit(tip);
	}
	
	@Override
	public void aduna(double valoare)throws ExceptieNumarMare, ExceptieNumarMic{
		
		if (valoare == Double.MAX_VALUE)
			throw new ExceptieNumarMare("valoare prea mare!");
		if (valoare == Double.MIN_VALUE)
			throw new ExceptieNumarMic("valoare prea mica!");
		this.venituriTotale += valoare;
	}
	
	@Override
	public void scade(double valoare)throws ExceptieNumarMic, ExceptieNumarMare{
		
		if (valoare == Double.MAX_VALUE)
			throw new ExceptieNumarMare("valoare prea mare!");
		if (valoare == Double.MIN_VALUE)
			throw new ExceptieNumarMic("valoare prea mica!");
		this.venituriTotale -=valoare;
	}

}
