package main;

import java.awt.BorderLayout;
import java.awt.Label;
import java.sql.Date;

import javax.swing.*;

import classes.*;



public class Main {
	
	
	static JButton butonAdauga = new JButton("Adauga");
	
	
	public static void deschideFereastraStart() {
		
		//singleton
		FereastraUnica fereastra = FereastraUnica.getInstance("pop up");
		fereastra.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fereastra.getContentPane().add(new Label("Bun venit!"), BorderLayout.CENTER);
		JButton adaugaVenit = new JButton("Adauga Venit");
		JButton adaugaCheltuiala = new JButton("Adauga Cheltuiala");
		adaugaVenit.addActionListener(new java.awt.event.ActionListener(){
			public void actionPerformed(java.awt.event.ActionEvent evt){
				deschideFereastraAdaugareVenit();
			}

		});
		adaugaCheltuiala.addActionListener(new java.awt.event.ActionListener(){
			public void actionPerformed(java.awt.event.ActionEvent evt){
				deschideFereastraAdaugareCheltuiala();
			}

		});
		fereastra.getContentPane().add(adaugaVenit, BorderLayout.WEST);
		fereastra.getContentPane().add(adaugaCheltuiala, BorderLayout.EAST);
		fereastra.setSize(600,400);
		fereastra.setVisible(true);
	}

	protected static void deschideFereastraAdaugareCheltuiala() {
		FereastraBuilder builder = new FereastraBuilder("Fereastra adaugare cheltuiala");
		
		FereastraCustom fereastraPrincipala = builder.adaugaButon(butonAdauga).buildFereastra();
		
		fereastraPrincipala.setVisible(true);
		
	}

	protected static void deschideFereastraAdaugareVenit() {
		FereastraBuilder builder = new FereastraBuilder("Fereastra adaugare venit");
		
		FereastraCustom fereastraPrincipala = builder.adaugaButon(butonAdauga).buildFereastra();
		
		fereastraPrincipala.setVisible(true);
		
	}

	public static void main(String[] args) {


		
		//Ferestre
		JButton butonLogin = new JButton("Login");
		butonLogin.addActionListener(new java.awt.event.ActionListener(){
			public void actionPerformed(java.awt.event.ActionEvent evt){
				deschideFereastraStart();
			}

		});
		//builder
		FereastraBuilder builder = new FereastraBuilder("Fereastra principala");
		
		FereastraCustom fereastraPrincipala = builder.adaugaButon(butonLogin).buildFereastra();
		
		fereastraPrincipala.setVisible(true);
		
		
		//Restul claselor
		int idBuget = 1;
		int idJudet = 1;
		Date data = new Date(2015,5,20);
		//factory
		Buget cheltuieli = new FactoryCheltuieli(idBuget,idJudet,data);
		Buget venituri = new FactoryVenituri(idBuget,idJudet,data);	
		Cheltuiala cheltuiala = (Cheltuiala) cheltuieli.createObject("cheltuiala de investitii");
		Venit venit = (Venit) venituri.createObject("venit propriu");
		double[] valori = {1,2,3};
		//strategy
		cheltuieli.setPrelucrare(new Suma());
		cheltuieli.setCheltuieliTotale(cheltuieli.getPrelucrare().prelucreaza(valori));
		System.out.println("Suma cheltuielilor este: " + cheltuieli.getCheltuieliTotale());
		
		//composite
		Teritoriu regiune = null;
		Teritoriu judet = null;
		Teritoriu judet2 = null;
		try {
			regiune = new RegiuneDeDezvoltare(1, "CENTRU");
			judet =  new Judet("Bucuresti");
			judet2= new Judet("Ilfov");
			regiune.add(judet);
			regiune.add(judet2);
			regiune.descriere();
			
			
			
		} catch (ExceptieValoareNegativa e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		

		
		
		
		
	}

}
