package teste;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import classes.Suma;

public class SumaTest {

	public Suma suma = null;
	public double[] date = { 1.0, 2.0, 3.0, 4.0 };
	
	@Before
	public void setUp() throws Exception {
		suma = new Suma();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPrelucreazaValoriNormale() {
		double sumaAsteptata =0.0;
		for (double valoare : date){
			sumaAsteptata += valoare;
		}
		
		
		assertEquals("Test prelucreaza - suma valori normale",sumaAsteptata,suma.prelucreaza(date), 0);
		
	}
	
	@Test
	public void testPrelucreazaNull() {
		
		
		assertEquals("Test prelucreaza - suma null",0.0,suma.prelucreaza(null),0);
		
	}

}
