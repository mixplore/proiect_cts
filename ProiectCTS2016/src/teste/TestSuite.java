package teste;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@SuiteClasses({FactoryCheltuieliTest.class,FactoryVenituriTest.class,RegiuneDeDezvoltareTest.class,
	SumaTest.class,MedieAritmeticaTest.class})
@RunWith(Suite.class)public class TestSuite {
	public TestSuite() {
		
	}

}
