package teste;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import classes.*;

public class RegiuneDeDezvoltareTest {

	
	public RegiuneDeDezvoltare regiune = null;
	String denumire;
	int idRegiune;
	
	@Before
	public void setUp() throws Exception {
		
		idRegiune = 0;
		denumire = "";
		regiune = new RegiuneDeDezvoltare(idRegiune,denumire);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRegiuneDeDezvoltareConstructorDoiParamValoriNormale() throws ExceptieValoareNegativa {
		int idRegiune = 1;
		String denumire = "Nord-Vest";
		regiune = new RegiuneDeDezvoltare(idRegiune,denumire);
		assertEquals("Test constructor doi parametri normali",idRegiune, regiune.getIdZona());
		assertEquals("Test constructor doi parametri normali",denumire, regiune.getDenumireZona());
	}
	
	@Test
	public void testRegiuneDeDezvoltareConstructorDoiParamIdNegativ() {
		int idRegiune = -1;
		String denumire = "Nord-Vest";
		try{
			regiune = new RegiuneDeDezvoltare(idRegiune,denumire);
			fail();
		}
		catch(ExceptieValoareNegativa ex){
			
		}
	}
	
	@Test
	public void testRegiuneDeDezvoltareConstructorDoiParamDenumireNull() throws ExceptieValoareNegativa {
		int idRegiune = 1;
		String denumire = null;
		regiune = new RegiuneDeDezvoltare(idRegiune,denumire);
		assertNull("Test constructor - denumire=null",regiune.getDenumireZona());		
		
	}


	@Test
	public void testSetIdZonaValoriNegative ()throws ExceptieValoareNegativa, ExceptieNumarMare  {
		idRegiune = -1;
		try{
			regiune.setIdZona(idRegiune);
			fail();
		}
		catch(ExceptieValoareNegativa ex){
			
		}
	}
	
	@Test
	public void testSetIdZonaValoriNormale() throws ExceptieNumarMare, ExceptieValoareNegativa {
		regiune.setIdZona(idRegiune);
		assertEquals("Test getter Id regiune/zona",idRegiune,regiune.getIdZona());
	}
	
	@Test
	public void testSetIdZonaValoriMari() throws ExceptieNumarMare{
		idRegiune = Integer.MAX_VALUE + 1;
		try{
			regiune.setIdZona(idRegiune);
			fail();
		}
		catch(ExceptieNumarMare ex){
			
		}
		catch(Exception e){
			
		}	
		
		
	}
	

	@Test
	public void testSetDenumireZonaNormala() {
		regiune.setDenumireZona(denumire);
		assertEquals("Test getter Id regiune/zona",denumire,regiune.getDenumireZona());
	}
	
	@Test
	public void testSetDenumireZonaNull() {
		denumire = null;
		regiune.setDenumireZona(denumire);
		assertNull("Test constructor - denumire=null",regiune.getDenumireZona());
	}


	@Test
	public void testAdd() {
		Teritoriu teritoriuNou = new Judet(idRegiune);
		regiune.add(teritoriuNou);
		assertEquals("Test add judet - verificare lista judete",1,regiune.getJudete().size());
		assertEquals("Test add judet",teritoriuNou,regiune.getChild(0));		
	}

	@Test
	public void testRemove() {
		Teritoriu teritoriuNou = new Judet(idRegiune);
		regiune.add(teritoriuNou);
		assertEquals("Test add judet - verificare marime lista judete",1,regiune.getJudete().size());
		regiune.remove(teritoriuNou);
		assertEquals("Test add judet - verificare lista judete",0,regiune.getJudete().size());
	}

	@Test
	public void testGetChild() {
		Teritoriu teritoriuNou = new Judet(idRegiune);
		regiune.add(teritoriuNou);
		Teritoriu teritoriuNou2 = new Judet(idRegiune);
		regiune.add(teritoriuNou2);
		Teritoriu teritoriuNou3 = new Judet(idRegiune);
		regiune.add(teritoriuNou3);
		assertEquals("Test add judet - verificare lista judete",3,regiune.getJudete().size());
		assertEquals("Test add judet",teritoriuNou,regiune.getChild(0));
		assertEquals("Test add judet",teritoriuNou2,regiune.getChild(1));
		assertEquals("Test add judet",teritoriuNou3,regiune.getChild(2));
	}

}
