package teste;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Scanner;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import classes.MedieAritmetica;

public class MedieAritmeticaTest {

	MedieAritmetica medie = null;
	double[] valori = new double[10];
	private File f=null;
    private Scanner sc=null;
    
	@Before
	public void setUp() throws Exception {
		
		 f = new File("Date.txt");
         sc = new Scanner(f);
         String linie=null;
         int i = 0;
         while (sc.hasNextLine()){
                 linie=sc.nextLine();
                 //String[] numere=linie.split(" ");
                 valori[i]=Double.parseDouble(linie);
                 i++;
         }
         
         medie = new MedieAritmetica();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test (timeout = 10)
	public void testPrelucreazaPerformanta() {
		
		medie.prelucreaza(valori);
	}
	
	@Test 
	public void testPrelucreaza() {
		
		double medieAsteptata = 0.0;
		for (double valoare : valori){
			medieAsteptata+=valoare;
		}
		medieAsteptata = medieAsteptata/valori.length;
		
		double medieObtinuta = medie.prelucreaza(valori);
		assertEquals("Test medie aritmetica",medieAsteptata,medieObtinuta,0.3);
	}

}
